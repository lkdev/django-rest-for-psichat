from channels.generic.websocket import WebsocketConsumer
import json
from django.conf import settings
from psiChat.chatModule.pattern_chat.chat import PatternMatching
from .models import Message, Chat

from rest_for_psichat.settings import graph


def predict(text):
    with graph.as_default():
        pr = settings.MODEL
        answer = pr.predict(text)
    return answer


def pattern_predict(text):
    pattern = PatternMatching()
    if pattern.intent == 1:
        answer = pattern.hello()
        pattern.intent += 1
        return answer
    else:
        answer = pattern.predict(text)
        pattern.intent += 1
        return answer


class SocketWeb(WebsocketConsumer):
    def connect(self):
        import time
        self.accept()
        self.chat = Chat.objects.create(name=time.strftime("%Y-%m-%d-%H.%M.%S", time.localtime()))
        self.send(json.dumps({"content": "Good day! ", "author": "BOT"}))

    def disconnect(self):
        pass

    def receive(self, text_data):

        text = text_data
        Message.objects.create(id_chat=self.chat, text=text, id_user='test')
        text = pattern_predict(text)
        if text == 0:
            text = predict(text_data)
            Message.objects.create(id_chat=self.chat, text=text, id_user='bot')
        text = json.dumps({"content": " Ответ бота: {}".format(text), "author": "BOT"})
        # text = str(text)
        if text:
            self.send(text)
