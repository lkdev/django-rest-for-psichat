from rest_framework import serializers

from psiChat.models import KeywordsOld


class KeywordsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = KeywordsOld
        fields = ('id_dialogue','word')
