from sim_module import morph_to_vect
from sim_module import text_utils
import gensim
import pymorphy2
import csv
import pandas
import pattern_chat.scenary_files
import pattern_chat.patterns

# phrase = 'Ничего Сижу работаю над дипломом Возникла небольшая проблема, лол Надо думать как решать'

# clear_sent = text_utils.get_clear_lines(phrase)
# tokens = text_utils.tokenize_line(clear_sent)
# morph = pymorphy2.MorphAnalyzer()
# model = gensim.models.KeyedVectors.load_word2vec_format("./sim_module/keyed_vect.w2v", unicode_errors='ignore',
#                                                        binary=True)
# new_phrase = []
# norm_form = [morph.normal_forms(token) for token in tokens]
# for token in tokens:
#    pos = morph.parse(token)[0].tag.POS
#    gend = morph.parse(token)[0].tag.gender
#    new_phrase.append(morph_to_vect.search_neighbors_2(token, pos, gend=gend, model=model, morph=morph))

# print(new_phrase)


# db = shelve.open("./data/key_word_to_index", 'n')
# db["fasilitator"] = ['поговорить', 'поделиться', 'рассказать']
# db.close()

# data = ["fasilitator, common, bairon".split(','), "проблема, произошло, случилось".split(","),
#        ['поговорить', 'поделиться', 'рассказать'], "сформулировать, беспокойство, тревога".split(",")]
# n_data = []
# for value in data[1:]:
#    n_data.append(dict(zip(data[0], value)))

# csv_dict_writer("./data/key_word_to_index.csv", data[0], n_data)
# with open("./data/key_word_to_index.csv", 'r') as f:
#    csv_dict_reader(f)
scenary = pattern_chat.scenary_files.KeyScenary()
step = pattern_chat.scenary_files.Scenario(key_word=scenary.get_scenario_name('проблема'))
print(step.get_step(step.sentence_return(step.key_word),1,0))

