"""
Поиск паттернов во входящем тексте от пользователя

"""
import sim_module.text_utils as utils
import json

# TODO:
# 1. Метод парсинга входного текста [Y]
# 2. Метод поиска шаблона для выбора сценария [Y]
# 3. Работа с shelve сценариями



def get_clear_sentence(raw_sentence: str) -> list:
    """
    Токенизируем входящий текст
    :param raw_sentence: text (str)
    :return: list of tokens
    """
    raw_sentence = utils.tokenize_sentence(raw_sentence)
    raw_sentence = [utils.get_clear_lines(sent, replace=True) for sent in raw_sentence]
    raw_sentence = ' '.join(raw_sentence)
    raw_sentence = utils.tokenize_line(raw_sentence)
    return raw_sentence

def read_scenary_json(path= '/data/key_word_to_index.json'):
    with open(path,'r'):
        json.loads()


def search(token: str, scenary_key: object) -> list or tuple:
    """
    Ищем токен в словаре. Возвращаем номер или None
    :param token: str
    :param scenary_key: dictionary of scenary
    :return: list or tuple (None or Yes, number of scenary)
    """
    import pattern_chat.scenary_files as scenary
    scenary_key = scenary.KeyScenary
    if token in scenary_key.values:
        return ["Yes", scenary_key.index_name(token)]
    else:
        return (None, "")


def search_pattern(tokens: list) -> list:
    """
    Ищем ключевое слово в ключах возвращаем номер сценария
    :param tokens: list of tokens
    :return: list of number
    """
    pos_pattern = []
    for token in tokens:
        if search(token)[0] is not None:
            pos_pattern.append(search(token)[1])
    return pos_pattern


