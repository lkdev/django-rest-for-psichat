from rest_framework import viewsets
import rest_framework.permissions as permissions
from psiChat.models import Dialogue
from psiChat.serializers import DialogueSerializer


class DialogueViewSet(viewsets.ModelViewSet):
    queryset = Dialogue.objects.all()
    serializer_class = DialogueSerializer
    permissions_classes = [permissions.AllowAny]

    def get_queryset(self):
        """
        This view should return a list of all records
        """
        return Dialogue.objects.all().order_by('name')
