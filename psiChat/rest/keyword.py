from rest_framework import viewsets

from psiChat.models import KeywordsOld
from psiChat.serializers import KeywordsSerializer


class KeywordsViewSet(viewsets.ModelViewSet):
    queryset = KeywordsOld.objects.all()
    serializer_class = KeywordsSerializer