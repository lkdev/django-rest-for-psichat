from rest_framework import viewsets

from psiChat.models import Message
from psiChat.serializers import MessageSerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
