from django.contrib import admin
from .models import Chat, Message, KeywordsOld, Dialogue, Replica, SentimentAnalis

class ChatAdmin(admin.ModelAdmin):
    fields = ['name']
admin.site.register(Chat, ChatAdmin)

class DialogueAdmin(admin.ModelAdmin):
    fields = ['name', 'description', 'number_steps', 'keywords']
admin.site.register(Dialogue, DialogueAdmin)

class MessageAdmin(admin.ModelAdmin):
    readonly_fields = ('date',)
    fields = ['text', 'id_user', 'id_chat']
admin.site.register(Message, MessageAdmin)

class KeywordsAdmin(admin.ModelAdmin):

    fields = ['id_dialogue', 'word']
admin.site.register(KeywordsOld, KeywordsAdmin)

class ReplicaAdmin(admin.ModelAdmin):

    fields = ['dialogue', 'text', 'number_sent']
admin.site.register(Replica, ReplicaAdmin)

class SentimentAdmin(admin.ModelAdmin):

    fields = ['id_message', 'result']
admin.site.register(SentimentAnalis, SentimentAdmin)