import Vue from 'vue'
import Vuex from 'vuex'
import ip_adress from './ip_adress'
const axios = require('axios');

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        socket: {
            isConnected: false,
            message: '',
            reconnectError: false,
            messagesList: []
        },
        auth:{
            authorized: localStorage.authorized,
            token: localStorage.token,
            username: localStorage.username,
            id: ''
        },
        admin: {
            dialogs: [],
            newDialog: {
                name:'',
                description:'',
                number_steps:'',
                key_word:''
            }
        },
    },
    getters: {
        newDialog: state => state.newDialog,
        dialogs: state => state.dialogs
    },
    mutations:{
        SOCKET_ONOPEN (state, event)  {
            Vue.prototype.$socket = event.currentTarget
            state.socket.isConnected = true
        },
        SOCKET_ONCLOSE (state, event)  {
            state.socket.isConnected = false
        },
        SOCKET_ONERROR (state, event)  {
            console.error(state, event)
        },
        // default handler called for all methods
        SOCKET_ONMESSAGE (state, message)  {
            console.log(message)
            state.socket.message = message
            state.socket.messagesList.push({author: message.author, content: message.content})
        },
        // mutations for reconnect methods
        SOCKET_RECONNECT(state, count) {
            console.info(state, count)
        },
        SOCKET_RECONNECT_ERROR(state) {
            state.socket.reconnectError = true;
        },
        MESSAGE_TO_WEBSOCKET(state, message){
            state.message = message;
        },
        LOGIN_TOKEN(state, payload){;
            state.auth.token = payload.token;
            localStorage.token = payload.token;

            state.auth.authorized = true;
            localStorage.authorized = true;
        },
        SET_USERNAME_BLYAT(state, payload) {
            //TODO: NOT BLYAT
            console.log(payload);
            state.auth.username = payload;
            localStorage.username = payload;
        },
        CLEAR_DATA(state, payload) {
            state.auth.username = '';
            state.auth.token = '';
            state.auth.authorized = '';

            localStorage.username = '';
            localStorage.token = '';
            localStorage.authorized = false;
        },
        SET_SCENARY (state, dialogs) {
            state.dialogs = dialogs
        },
        ADD_SCENARY (state, dialogObject) {
            state.dialogs.push(dialogObject)
        },
        CLEAR_NEW_SCENARY(state) {
            state.newDialog = {name:'',
                description:'',
                number_steps:'',
                key_word:''
            }
        }

    },
    actions: {
        loadChat: function(context, data) {
            /* axios.setHeader('Access-Control-Allow-Headers',
              'Access-Control-Allow-Headers, Origin,OPTIONS,Accept,Authorization, X-Requested-With,' +
                      ' Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers'); */
            axios({
                method: 'get',
                url: 'http://' + ip_adress() + '/api/message',
                headers: { Authorization: 'Token ' + context.state.auth.token },
            }).then(responce =>
                responce.data.forEach(data => {
                    this.chatMessages.push({ user: data.id_user, content: data.text })
                })
            )
        },
        sendMessage: function(context, message) {

            Vue.prototype.$socket.send(message);
            context.state.socket.messagesList.push({author: context.state.auth.username, content: message})

        },
        listMessage: function (context, message) {

        },
        login: function(context, data) {
            axios({
                method: 'post',
                url: 'http://' + ip_adress() + '/auth/token/create/',
                headers: {'Content-Type': 'multipart/form-data'},
                data: data
            }).then(response => {
                console.log('LOGIN RESPONSE')
                context.commit('LOGIN_TOKEN', {token: response.data.auth_token})
                console.log(response)
                context.state.auth.authorized = true;
                localStorage.authorized = true;
            })
        },
        logout: function(context, data) {
            axios({
                method: 'post',
                url: 'http://' + ip_adress() + '/auth/token/destroy/',
                headers: {'Authorization': 'Token ' + context.state.auth.token}
            }).then(response => {
                console.log('clear data?')
                context.commit('CLEAR_DATA')
            })
        },
        loadDialogs ({ commit }) {
            axios({
                method:'get',
                url: 'http://' + ip_adress() + '/api/dialogue/',
                headers: {'Authorization': 'Token' + context.state.auth.token}
            }).then(dialogs => {
                commit('SET_DIALOGS', dialogs)
            })
        },
        addDialog ({ commit, state }) {

            const todo = {
                name: state.newDialog.name,
                description: state.newDialog.description,
                number_steps: state.newDialog.number_steps,
                key_word: state.newDialog.key_word
            };
             axios({
                method:'post',
                 url: 'http://' + ip_adress() + '/api/dialogue/',
                 headers: {'Authorization': 'Token' + context.state.auth.token}
             }).then(_ => {
                 commit('ADD_DIALOG', todo)
             })
        },
        clearNewTodo ({ commit }) {
            commit('CLEAR_NEW_DIALOG')
        },
    }
})
