"""rest_for_psichat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework import routers

from psiChat.rest.dialogue import DialogueViewSet
from psiChat.rest.chat import ChatViewSet
from psiChat.rest.message import MessageViewSet
from psiChat.rest.keyword import KeywordsViewSet
from psiChat.rest.replika import ReplicaViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'dialogue', DialogueViewSet, base_name='dialogue')
router.register(r'chat', ChatViewSet, base_name='chats')
router.register(r'message', MessageViewSet, base_name='message')
router.register(r'keyword', KeywordsViewSet, base_name='keyword')
router.register(r'replica', ReplicaViewSet, base_name='replica')

urlpatterns = [
    path('', TemplateView.as_view(template_name='base.html')),
    path('admin/', admin.site.urls),
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),
    path('auth/', include('djoser.urls.jwt')),
    url('api/', include(router.urls)),
]
