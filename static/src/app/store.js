import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    dialogs: [],
    newDialog: {
        name:'',
        description:'',
        number_steps:'',
        key_word:''
    }
  },
  getters: {
    newDialog: state => state.newDialog,
    dialogs: state => state.dialogs
  },
  mutations: {
    SET_DIALOGS (state, dialogs) {
      state.dialogs = dialogs
    },
    ADD_DIALOG (state, dialogObject) {
      state.dialogs.push(dialogObject)
    },
    CLEAR_NEW_DIALOG (state) {
      state.newDialog = {name:'',
                         description:'',
                         number_steps:'',
                         key_word:''
                        }
  }
  },
  actions: {
    loadDialogs ({ commit }) {
      axios
        .get('/api/dialogue')
        .then(r => r.data)
        .then(dialogs => {
          commit('SET_DIALOGS', dialogs)
        })
    },
    addDialog ({ commit, state }) {

      const todo = {
        name: state.newDialog.name,
        description: state.newDialog.description,
        number_steps: state.newDialog.number_steps,
        key_word: state.newDialog.key_word
      };
      axios.post('/api/dialogue', todo).then(_ => {
        commit('ADD_DIALOG', todo)
      })
    },
    clearNewTodo ({ commit }) {
      commit('CLEAR_NEW_DIALOG')
    },
  }
})